package gamestuff.demotest;

import java.awt.Graphics;

public class Logic extends Window implements Runnable{
    
    TestLevel levelone = new TestLevel();
    TestLevel levelcloud = new TestLevel();
    
    Player player1 = new Player();
    Player player2 = new Player();
    
    boolean running;
    
    Thread thLogic;
    
    static int delay = 20;

    public void run(){
        thLogic = new Thread(new Logic());
        
    try{
        thLogic.start();
    }catch(Exception e) {e.printStackTrace();}
    }
    
    public Logic() {
       running = true;
       addKeyListener(player1);
       addKeyListener(player2);
       logicLoop();
    }
    
    public void logicLoop(){
    repaint();
    player2.x+=100;
    player2.y+=100;
    player2.moveKeys[0] = 38;
    player2.moveKeys[1] = 40;
    player2.moveKeys[2] = 39;
    player2.moveKeys[3] = 37;
        while(running){
            player1.spawn(true);
            player2.spawn(true);
            repaint(0,0,800,600);
            deltaTime();
        }
    }
    
    public void deltaTime(){
    //Currently only acts as a loop delay not actual deltTime funtionality
    try{thLogic.sleep(delay);}catch(Exception e){e.printStackTrace();}
    }
    
    public void paint(Graphics g){
        levelone.level(g);
        if(levelcloud.x >= 800)levelcloud.x=-596;
        levelcloud.x+=2;
        player1.paintComponent(g);
        player2.paintComponent(g);
        g.drawImage(levelone.levelContents[3],levelone.x,9,null);
        g.drawImage(levelcloud.levelContents[4],levelcloud.x+300,100,null);
    }
    
    @Override
    public void update(Graphics g){
         paint(g);
    }
}