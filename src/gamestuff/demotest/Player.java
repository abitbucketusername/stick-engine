package gamestuff.demotest;

import java.awt.image.BufferedImage;
import java.awt.Graphics;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import java.io.File;
import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class Player extends JPanel implements KeyListener {
    
    final String stickmanSprite = "stickman";
    final int SPRITEFRAMES = 13; //Frames + 1
    int frameCounter = 0;
    int moveKeys[] = {87,83,68,65,32};
    boolean up, down, right, left, jump, fly, jumping;
    int upCon, downCon, rightCon, leftCon;
    int max;
    int x, y;
    double jumpCounter = 4;
    
    BufferedImage[] stickAni = new BufferedImage[SPRITEFRAMES];
    BufferedImage stickman;
    
    public void loadImages(){
        try {
            for(int i = 0; i <= stickAni.length; i++){
            stickAni[i] = ImageIO.read(new File("res/stickman_"+i+".png"));
            }
        } catch (Exception e) {}
    }
    
    public Player() {
        loadImages();
        stickman = stickAni[0];
    }
    
    public void paintComponent(Graphics g){
    super.paintComponent(g);
    drawPlayer(g);
    }
    
    public void drawPlayer(Graphics g){
    g.drawImage(stickman, x, y,null);
    }
    
    public void spawn(boolean isOn){
    this.gravity(isOn);
    this.move();
    this.moveAnimation();
    }
    
    public void move() {
    if(up && y >= Window.ORIGIN+20)y-=5;
    if(down && y != Window.FRAMEHEIGHT-50)y+=10;
    if(right && x != Window.FRAMEWIDTH-40)x+=5;
    if(left && x != Window.ORIGIN-10)x-=5;
    if(jump && y != Window.ORIGIN+20){
        if(fly){jump=false;} 
        if(y == 550){jump=true;}
        jumpCounter+=0.1;
        y+=(int) Math.floor((Math.sin(jumpCounter) + Math.cos(jumpCounter)) * 20);
        if(jumpCounter > 6){jumpCounter=4;}
    }
}
    
    public void gravity(boolean isOn){
        if(isOn){
            if(!up && y >= Window.FRAMEHEIGHT-50)y = Window.FRAMEHEIGHT-50;
            if(!up && !down && y != Window.FRAMEHEIGHT-50)y+=10;
        }
    }
    
    public void moveAnimation() {
        
    if(!right && !left || right && left)stickman = stickAni[0];
    if(up)stickman = stickAni[11];
    if(down && !jump && y != Window.FRAMEHEIGHT-50)stickman = stickAni[12];
    if(down && left && !jump && y != Window.FRAMEHEIGHT-50)stickman = stickAni[10];
    if(down && right && !jump && y != Window.FRAMEHEIGHT-50)stickman = stickAni[9];
    if(jump)stickman = stickAni[1];
    
    if(left && up)stickman = stickAni[8];
    if(right && up)stickman = stickAni[4];
    
        if(right && !jump && !up && !left && !down || down && right && y == Window.FRAMEHEIGHT-50){
            if(frameCounter == 3)frameCounter = 0;
            frameCounter++;
            
            if(frameCounter != 3){
            switch(frameCounter){
                case 0: stickman = stickAni[1];break;
                case 1: stickman = stickAni[2];break;
                case 2: stickman = stickAni[3];break;
            }
            }
        }
        if(left && !jump && !up && !right && !down || down && left && y == Window.FRAMEHEIGHT-50){
            if(frameCounter == 3)frameCounter = 0;
            frameCounter++;
            
            if(frameCounter != 3){
            switch(frameCounter){
                case 0: stickman = stickAni[5];break;
                case 1: stickman = stickAni[6];break;
                case 2: stickman = stickAni[7];break;
            }
            }
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if(e.getKeyCode() == moveKeys[0]){up=true;fly=true;}
        if(e.getKeyCode() == moveKeys[1])down=true;
        if(e.getKeyCode() == moveKeys[2])right=true;
        if(e.getKeyCode() == moveKeys[3])left=true;
        if(e.getKeyCode() == moveKeys[4])if (y == 550)jump=true;
    }

    @Override
    public void keyReleased(KeyEvent e){
        if(e.getKeyCode() == moveKeys[0]){up=false;fly=false;}
        if(e.getKeyCode() == moveKeys[1])down=false;
        if(e.getKeyCode() == moveKeys[2])right=false;
        if(e.getKeyCode() == moveKeys[3])left=false;
        if(e.getKeyCode() == moveKeys[4]){jump=false;jumpCounter=4;}
    }
    
    //Unused
    public void keyTyped(KeyEvent e){}
}
   
