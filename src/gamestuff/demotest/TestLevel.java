package gamestuff.demotest;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;

public class TestLevel {
    int objectsOnLevel=5;
    int x = 0;
    
    boolean outside;
    
    BufferedImage levelContents[] = new BufferedImage[objectsOnLevel];
    TestLevel(){loadContents();}
    public void level(Graphics g){
    if(x >= 800)x=-296;
    x++;
    g.drawImage(levelContents[0],0,0,null);
    g.drawImage(levelContents[3],x,200,null);
    }
    
    public void loadContents(){
    try{
        levelContents[0] = ImageIO.read(new File("TestLevel/CompiledMap.png"));
        levelContents[1] = ImageIO.read(new File("Testlevel/SkyBox.png"));
        levelContents[2] = ImageIO.read(new File("TestLevel/Ground.png"));
        levelContents[3] = ImageIO.read(new File("TestLevel/Cloud.png"));
        levelContents[4] = ImageIO.read(new File("Testlevel/SpecialCloud.png"));
    }catch(Exception e){e.printStackTrace();}
    }
}
