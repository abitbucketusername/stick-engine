package gamestuff.demotest;

import java.awt.Color;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JFrame;

public class Window extends JFrame{
    
    private final String TITLE = "Ace";
    static final int ORIGIN = 0;
    static final int FRAMEWIDTH = 800, FRAMEHEIGHT = 600;
    
    public Window(){
        ImageIcon icon = new ImageIcon("res/stickman_0.png");
        Image iIcon = icon.getImage();
        setIconImage(iIcon);
        setTitle(TITLE);
        setSize(FRAMEWIDTH,FRAMEHEIGHT);
        setResizable(false);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBackground(Color.BLACK);
    }
    
    public static void main(String[] args){
		//Keep either new Logic() or new Sandbox commented out
        new Logic();
        //new Sandbox(); //Original code Logic is based off of, both completely different entities
    }
}
